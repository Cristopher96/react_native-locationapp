// all imports needed
import { useScreens } from 'react-native-screens';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Login from './src/screens/Login';
import Home from './src/screens/Home';
import QR from './src/screens/QR';
import Auth from './src/screens/Auth';

/*
    * method used to reduce memory consumed
*/
useScreens();

/* 
    * creating the routes of our app 
*/
const AppStack = createStackNavigator({
    Home,
}, {
    defaultNavigationOptions: {
        header: null,
        gesturesEnabled: false,
    }
});

const AuthStack = createStackNavigator({
    Login,
    QR,
}, {
    defaultNavigationOptions: {
        header: null,
        gesturesEnabled: false,
    }
});

/*
    * exporting the app
*/
export default createAppContainer(
    createSwitchNavigator({
        AuthLoading: Auth,
        App: AppStack,
        Auth: AuthStack,
    }, {
        initialRouteName: 'AuthLoading',
    })
);