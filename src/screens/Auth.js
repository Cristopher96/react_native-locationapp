// all imports needed
import React, { Component } from 'react';
import { AsyncStorage, StatusBar, StyleSheet, Image } from 'react-native';
import { mapping } from '@eva-design/eva';
import { theme } from '../Themes';
import { Layout, ApplicationProvider, Spinner } from 'react-native-ui-kitten';

// create and export the component
export default class Auth extends Component {
    /*
        * as the component gets mounted we execute this function
    */
    componentWillMount = () => {
        this._bootstrapAsync();
    }

    /*
        * in this method we validate if the user is logged in the app or not
        * we do this by checking if there is user session information in the local storage
        * if its true we navigate the user to the home screen
        * if not we navigate it to the login screen
    */
    _bootstrapAsync = async () => {
        const userToken = await AsyncStorage.getItem('user');
        this.props.navigation.navigate( userToken ? 'Home' : 'Login');
    }

    /*
        * rendeering the component's view
    */
    render() {
        return (
            <React.Fragment>
                <ApplicationProvider mapping={mapping} theme={theme}>
                    <StatusBar barStyle="dark-content" />
                    <Layout style={styles.container}>
                        <Image style={styles.img} source={require('../../assets/icon.png')}/>
                        <Spinner style={styles.spinner} />
                    </Layout>
                </ApplicationProvider>
            </React.Fragment>
        );
    }
}

// just some simple styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    img: {
        marginTop: 20,
        width: 70,
        height: 70,
        marginBottom: 10,
    },
    spinner: {
        color: '#1A74F2',
    },
});

