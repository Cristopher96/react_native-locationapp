// all imports needed
import React, { Component } from 'react';
import { StatusBar, StyleSheet, TouchableOpacity, Dimensions, Alert, AsyncStorage, Platform } from 'react-native';
import { mapping } from '@eva-design/eva';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import { Button, ApplicationProvider, Layout, Icon, IconRegistry } from 'react-native-ui-kitten';
import { theme } from '../Themes';
import Loader from '../components/Loader';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';

/*
    * get the button's icon ready before render it
*/
const logoutIcon = (style) => (
    <Icon {...style} name="log-out-outline" />
);

// creating and exporting the component
export default class Home extends Component {
    // id of the interval
    intervalID = 0;

    // component state
    state = {
        camera: {
            center: {
              latitude: 0,
              longitude: 0,
            },
            pitch: 10,
            altitude: 0,
            heading: 0,
            zoom: 18,
        },
        location: null,
        userId: null,
        deviceId: null,
        loading: false,
    }

    /*
        * first we get the user session to set some values in the state
        * then we send user location info to the server and create an interval to send this info each second
    */
    componentWillMount = async () => {
        await this._getLocationAsync();
        await this._getUserToken();
    }

    /*
        * stop the interval before leaving this screen to prevent memory leaks
    */
    componentWillUnmount = () => {
        clearInterval(this.intervalID);
    }

    /*
        * get the user session information from the local storage and save as a states
    */
    _getUserToken = async () => {
        let userToken = await AsyncStorage.getItem('user');
        let user = JSON.parse(userToken);
        this.setState({
            userId: user.userId,
            deviceId: user.deviceId,
        });
    }

    /*
        * prompt the user to confirm logout
        * if the user hit the "Cancel" button it just close the prompt
        * if the "Yes" button is hit we execute the logout function
    */
    confirmLogut = () => {
        Alert.alert('Log out', 'Are you sure you want to log out?', [
            { text: 'Cancel', style:'cancel' },
            { text: 'Yes', onPress: this.logout }
        ], {cancelable: false});
    }

    /*
        * deletes the user session information from the local storage
        * and return the user to the login screen
    */
    logout = async () => {
        this.setState({loading: true});
        await AsyncStorage.removeItem('user');
        this.props.navigation.navigate("Auth");
    }

    /*
        * first gets access to location
        * if access is accepted get the user location and set some states value
        * if not prompt a user with an error
    */
    _getLocationAsync = async () => {
        let { status } = await Permissions.askAsync(Permissions.LOCATION);
        if ( status !== 'granted') {
            Alert.alert('Error', 'Permissions to access location was denied', [{text: 'OK'}], {cancelable: false});
            clearInterval(this.intervalID);
        }
        let location = await Location.getCurrentPositionAsync();
        this.setState({
            location,
            camera: {
                center: {
                    latitude: location.coords.latitude,
                    longitude: location.coords.longitude
                },
                pitch: 10,
                altitude: location.coords.altitude,
                heading: location.coords.heading,
                zoom: 18,
            }
        });
    }

    /*
        * receive an object as params and send it to the server
    */
    _sendData = async (data) => {
        fetch('http://192.168.0.136:3005/save_coordinates', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
        }).then(resp => resp.json()).then(respJson => {
            
        });
    }

    updateUserLocation = (coordinate) => {
        this.setState({
            camera: {
                center: {
                    latitude: coordinate.latitude,
                    longitude: coordinate.longitude,
                },
                pitch: 10,
                altitude: coordinate.altitude,
                heading: this.state.camera.heading,
                zoom: 18, 
            }
        });
        let data = {
            longitude: coordinate.longitude,
            latitude: coordinate.latitude,
            id_user: this.state.userId,
            id_device: this.state.deviceId,
            otros: {
                accuracy: coordinate.accuracy,
                altitude: coordinate.altitude,
                altitudeAccuracy: coordinate.altitudeAccuracy,
                heading: this.state.camera.heading,
                speed: coordinate.speed,
                timestamp: coordinate.timestamp,
            }
        }
        this._sendData(data);
    }

    /*
        * rendering the screen view
    */
    render() {
        return (
            <React.Fragment>
                <IconRegistry icons={EvaIconsPack} />
                <ApplicationProvider mapping={mapping} theme={theme}>
                    <StatusBar barStyle="dark-content" />
                    <Layout style={styles.container}>
                        <Loader loading={this.state.loading} />
                        <MapView
                            provider={PROVIDER_GOOGLE}
                            style={styles.mapStyle}
                            camera={this.state.camera}
                            maxZoomLevel={this.state.camera.zoom}
                            showsCompass={false}
                            zoomEnabled={false}
                            zoomTapEnabled={false}
                            rotateEnabled={false}
                            pitchEnabled={false}
                            scrollEnabled={false}
                            showsUserLocation={true}
                            onUserLocationChange={locationChangeResult => this.updateUserLocation(locationChangeResult.nativeEvent.coordinate)}
                        >
                            <Marker 
                                coordinate={this.state.camera.center}
                            />
                        </MapView>
                        <TouchableOpacity style={styles.overlay}>
                            <Button
                                style={styles.btn}
                                icon={logoutIcon}
                                onPress={this.confirmLogut}
                            />
                        </TouchableOpacity>
                    </Layout>
                </ApplicationProvider>
            </React.Fragment>
        );
    }
}

// just some simple styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    overlay: {
        position: 'absolute',
        top: 15,
        right: 10,
    },
    btn: {
        backgroundColor: '#f04141',
        borderColor: '#f04141',
        height: 40,
        width: 40,
        borderRadius: 20,
        marginEnd: 10,
        marginVertical: Platform.OS === 'android' ? Constants.statusBarHeight : 10,
    },
    mapStyle: {
        marginTop: Platform.OS === 'android' ? Constants.statusBarHeight + 15 : 0,
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
    },
});
