// all imports needed
import React, { Component } from 'react';
import { StyleSheet, Keyboard, TouchableWithoutFeedback, StatusBar, Alert, AsyncStorage, Dimensions } from 'react-native';
import { mapping } from '@eva-design/eva';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import { Input, Layout, Button, Icon, ApplicationProvider, IconRegistry } from 'react-native-ui-kitten';
import { theme } from '../Themes';
import Constants from 'expo-constants';
import * as Device from 'expo-device';
import Loader from '../components/Loader';

/*
    * get the button's icon ready before render it
*/
const loginIcon = (style) => ( 
    <Icon {...style } name="log-in-outline" />
);

// creating and exporting the component
export default class Login extends Component {
    // component state
    state = {
        code: '',
        style: '',
        loading: false,
    }

    /*
        * set the state of the component with the input value
    */
    newInputValue = value => {
        this.setState({
            code: value,
        });
    }

    /*
        * this method get the input value and when we hit the log in button
        * if the input is empty prompt the user with an error,
        * if not get value of the input and create an object to send it to the server to validate the login
    */
    login = () => {
        Keyboard.dismiss();
        if (this.state.code.length === 0) {
            Alert.alert('Error', 'You must fill all the fields.', [{text: 'OK'}], {cancelable: false});
        } else if (this.state.code.length > 0) {
            const body = {
                code: this.state.code,
                imei: Constants.deviceId,
                marca: Constants.deviceName,
                otros: {
                    brand: Device.brand,
                    os: Device.osName,
                    osVersion: Device.osVersion,
                }
            }
            this._sendData(body);
        }      
    }

    /*
        * receive an object as paramert and send it to the server to validate the login
        * if the server returns true, we save the session in the localStorage and navigate the user to Home
        * if the server returns false we just let the user knows there is an error with the credentials
    */
    _sendData = (body) => {
        this.setState({loading: true});
        fetch('http://192.168.0.136:3005/asociar_dispositivos', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
        }).then(resp => resp.json()).then(respJson => {
            if (respJson.success) {
                let user = {
                    userId: respJson.id_user,
                    deviceId: respJson.id_devices,
                }
                this.setState({loading: true});
                AsyncStorage.setItem('user', JSON.stringify(user));
                this.props.navigation.navigate("App");
            } else {
                Alert.alert('Error', 'Error login in', [
                    {text: 'OK', onPress: () => this.setState({loading: false})}
                ], {cancelable: false});
            }
        });
    }
    /*
        * navigate the user to login scanning a qr code
    */
    qr = () => {
        this.props.navigation.navigate("QR");
    }

    /*
        * change the input's style when focus
    */
    focused = () => {
        this.setState({
            style: styles.focusedInput
        });
    }

    /*
        * change the input's style when blur
    */
    unfocused = () => {
        this.setState({
            style: ''
        });
    }

    /*
        * rendering the screen view
    */
    render() {
        return ( 
            <React.Fragment>
                <IconRegistry icons = { EvaIconsPack } /> 
                <ApplicationProvider mapping = { mapping } theme = { theme } >
                    <StatusBar barStyle = "dark-content" />
                    <Layout style = { styles.mainContainer }>
                        <Loader loading={this.state.loading} />
                        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                            <Layout style = { styles.container }>
                                <Input style = { this.state.style } placeholder = "Code" value = { this.state.code } 
                                onChangeText = { this.newInputValue } onFocus = { this.focused } onBlur = { this.unfocused } 
                                keyboardType="number-pad"
                                /> 
                                <Button style = { styles.btn } icon = { loginIcon } onPress = { this.login }>
                                    Log in
                                </Button> 
                                <Button style={{marginTop: 15}} appearance = "ghost" onPress={this.qr}>
                                    Or log in scanning a QR code 
                                </Button> 
                            </Layout> 
                        </TouchableWithoutFeedback>
                    </Layout>
                </ApplicationProvider> 
            </React.Fragment>
        );
    }
}

// just some simple styles
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
    },
    container: {
        paddingHorizontal: 20,
        justifyContent: 'center',
        height: Dimensions.get('window').height,
    },
    focusedInput: {
        borderColor: '#1A74F2',
    },
    btn: {
        marginTop: 5,
        backgroundColor: '#1A74f2',
        borderColor: '#1A74F2',
    },
});