// all imports needed
import React, { Component } from 'react';
import { StyleSheet, AsyncStorage, Alert } from 'react-native';
import { Layout, Button, Text, ApplicationProvider } from 'react-native-ui-kitten';
import * as Permissions from 'expo-permissions';
import { BarCodeScanner } from 'expo-barcode-scanner';
import { mapping } from '@eva-design/eva';
import { theme } from '../Themes';
import Loader from '../components/Loader';
import Constants from 'expo-constants';
import * as Device from 'expo-device';

// creating and exporting the component
export default class QR extends Component {
    /*
        * component state
    */
    state = {
        hasCameraPermission: null,
        scanned: false,
        loading: false,
    }

    /*
        * When the component gets mounted ask inmediately for camera permissions
        * used in the future to scan a qr code
    */
    componentDidMount = () => {
        this.getPermissionsAsync();
    }

    /*
        * prompt the user to enable camera permissions
    */
    getPermissionsAsync = async () => {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({
            hasCameraPermission: status === 'granted'
        });
    }

    /*
        * method to get the data inside a qr code,
        * create an object and send it to the server to validate the login
    */
    handlerBarCodeScanned = ({ data }) => {
        this.setState({
            scanned: true,
            loading: true,
        });
        const body = {
            code: data,
            imei: Constants.deviceId,
            marca: Constants.deviceName,
            otros: {
                brand: Device.brand,
                os: Device.osName,
                osVersion: Device.osVersion,
            }
        }
        this._sendData(body);
    }

    /*
        * receive an object as paramert and send it to the server to validate the login
        * if the server returns true, we save the session in the localStorage and navigate the user to Home
        * if the server returns false we just let the user knows there is an error with the credentials
    */
    _sendData = (body) => {
        fetch('http://192.168.0.136:3005/asociar_dispositivos', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
        }).then(resp => resp.json()).then(respJson => {
            if (respJson.success) {
                let user = {
                    userId: respJson.id_user,
                    deviceId: respJson.id_devices,
                }
                AsyncStorage.setItem('user', JSON.stringify(user));
                this.props.navigation.navigate("App");
            } else {
                Alert.alert('Error', 'Error login in', [
                    {text: 'OK', onPress: () => this.setState({scanned: false ,loading: false})}
                  ], {cancelable: false});
            }
        });
    }

    /*
        * when in the camera view if the users hits the cancel button we take it back to the login page
    */
    cancel = () => {
        this.props.navigation.navigate("Login");
    }

    /*
        * rendering the screen view
    */
    render() {
        const { hasCameraPermission, scanned } = this.state;

        // validating the app has camera access
        if (hasCameraPermission === null) {
            return (
                <ApplicationProvider mapping={mapping} theme={theme}>
                    <Layout style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                        <Text>Requesting for camera permission.</Text>
                    </Layout>
                </ApplicationProvider>
            );
        }
        if (hasCameraPermission === false) {
            return (
                <ApplicationProvider mapping={mapping} theme={theme}>
                    <Layout style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                        <Text>No access to camera.</Text>
                        <Button
                            style={{marginTop: 15}}
                            onPress={this.cancel}
                        >
                            Cancel
                        </Button>
                    </Layout>
                </ApplicationProvider>
            );
        }
        
        // if the app has camera access we render the camera view and the cancel button
        return (
            <React.Fragment>
                <ApplicationProvider mapping={mapping} theme={theme}>
                    <Layout style={styles.container}>
                        <Loader loading={this.state.loading} />
                        <BarCodeScanner 
                            onBarCodeScanned={scanned ? undefined : this.handlerBarCodeScanned}
                            style={StyleSheet.absoluteFillObject}
                        />
                        <Button
                            style={{marginBottom: 15}}
                            onPress={this.cancel}
                        >
                            Cancel
                        </Button>
                    </Layout>
                </ApplicationProvider>
            </React.Fragment>
        );
    }
}

// just some simple styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
});